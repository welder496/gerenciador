package br.edu.gerenciador.interfaces;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.edu.gerenciador.model.UnidadeJudiciaria;
import br.edu.gerenciador.model.UnidadeJudiciariaAggregate;

@ManagedBean
@ViewScoped
public class TestarTribunalBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<String,String> segmentosJudiciariosMap;
	private Map<String,String> unidadesJudiciariasMap;
	private String segmentoJudiciario;
	private String unidadeJudiciaria;
	private Map<String,Map<String,String>> data = new HashMap<String, Map<String,String>>();	
	
	@PostConstruct
	public void init() {
		segmentosJudiciariosMap = new HashMap<String,String>();
		for (String str: UnidadeJudiciariaAggregate.listarSegmentosJudiciarios()) {
			segmentosJudiciariosMap.put(str, str);
			System.out.println("Segmento: "+str);
			unidadesJudiciariasMap = new HashMap<String,String>();
			for (UnidadeJudiciaria uj: UnidadeJudiciariaAggregate.listarUnidadesJudiciarias()) {
				if (uj.getSegmentoJudiciario().getDescricao().equals(str)) {
					unidadesJudiciariasMap.put(uj.getNome(), uj.getNome());
					System.out.println(uj.getNome());
				}
			}
			data.put(str, unidadesJudiciariasMap);
		}
	}
	
	public Map<String, String> getSegmentosJudiciariosMap() {
		return segmentosJudiciariosMap;
	}

	public void setSegmentosJudiciariosMap(Map<String, String> segmentosJudiciariosMap) {
		this.segmentosJudiciariosMap = segmentosJudiciariosMap;
	}

	public Map<String, String> getUnidadesJudiciariasMap() {
		return unidadesJudiciariasMap;
	}

	public void setUnidadesJudiciariasMap(Map<String, String> unidadesJudiciariasMap) {
		this.unidadesJudiciariasMap = unidadesJudiciariasMap;
	}

	public String getSegmentoJudiciario() {
		return segmentoJudiciario;
	}

	public void setSegmentoJudiciario(String segmentoJudiciario) {
		this.segmentoJudiciario = segmentoJudiciario;
	}

	public String getUnidadeJudiciaria() {
		return unidadeJudiciaria;
	}

	public void setUnidadeJudiciaria(String unidadeJudiciaria) {
		this.unidadeJudiciaria = unidadeJudiciaria;
	}

	public Map<String, Map<String, String>> getData() {
		return data;
	}
	
	public void onChangeSegmento() {
		System.out.println(segmentoJudiciario);
		if (segmentoJudiciario != null && !segmentoJudiciario.equals("")) {
			unidadesJudiciariasMap = data.get(segmentoJudiciario);
		} else {
			unidadesJudiciariasMap = new HashMap<String,String>();
		}
	}
	
}
