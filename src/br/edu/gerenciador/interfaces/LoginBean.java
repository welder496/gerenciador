package br.edu.gerenciador.interfaces;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.edu.gerenciador.model.Usuario;
import br.edu.gerenciador.persistence.UsuarioRepository;

@ManagedBean(name = "LoginBean")
@ViewScoped
public class LoginBean {

	private Usuario usuario = new Usuario();
	private UsuarioRepository usuarioRepo = new UsuarioRepository();
	
	
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String envia() {
        
	    usuario = usuarioRepo.getUsuario(usuario.getNome(), usuario.getSenha());
	    if (usuario == null) {
	      usuario = new Usuario();
	      FacesContext.getCurrentInstance().addMessage(
	         null,
	         new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
	           "Erro no Login!"));
	      return "index";
	    } else {
	          return "inicial";
	    }
	         
	         
	}	
	
}
