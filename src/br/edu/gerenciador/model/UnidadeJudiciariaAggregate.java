package br.edu.gerenciador.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UnidadeJudiciariaAggregate {

	private static final List<UnidadeJudiciaria> unidadesJudiciarias = new ArrayList<UnidadeJudiciaria>();
	
	static {
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.STF,0,"STF","Supremo Tribunal Federal"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.CNJ,0,"CNJ","Conselho Nacional de Justiça"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.STJ,0,"STJ","Superior Tribunal de Justiça"));

		//TRFs
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,1,"TRF1","Tribunal Regional Federal da 1ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,2,"TRF2","Tribunal Regional Federal da 2ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,3,"TRF3","Tribunal Regional Federal da 3ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,4,"TRF4","Tribunal Regional Federal da 4ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,5,"TRF5","Tribunal Regional Federal da 5ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JF,90,"CJF","Conselho da Justiça Federal"));
		
		//JT
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,0,"TST","Tribunal Superior do Trabalho"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,1,"TRT1","Tribunal Regional do Trabalho da 1ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,2,"TRT2","Tribunal Regional do Trabalho da 2ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,3,"TRT3","Tribunal Regional do Trabalho da 3ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,4,"TRT4","Tribunal Regional do Trabalho da 4ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,5,"TRT5","Tribunal Regional do Trabalho da 5ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,6,"TRT6","Tribunal Regional do Trabalho da 6ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,7,"TRT7","Tribunal Regional do Trabalho da 7ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,8,"TRT8","Tribunal Regional do Trabalho da 8ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,9,"TRT9","Tribunal Regional do Trabalho da 9ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,10,"TRT10","Tribunal Regional do Trabalho da 10ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,11,"TRT11","Tribunal Regional do Trabalho da 11ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,12,"TRT12","Tribunal Regional do Trabalho da 12ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,13,"TRT13","Tribunal Regional do Trabalho da 13ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,14,"TRT14","Tribunal Regional do Trabalho da 14ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,15,"TRT15","Tribunal Regional do Trabalho da 15ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,16,"TRT16","Tribunal Regional do Trabalho da 16ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,17,"TRT17","Tribunal Regional do Trabalho da 17ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,18,"TRT18","Tribunal Regional do Trabalho da 18ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,19,"TRT19","Tribunal Regional do Trabalho da 19ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,20,"TRT20","Tribunal Regional do Trabalho da 20ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,21,"TRT21","Tribunal Regional do Trabalho da 21ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,22,"TRT22","Tribunal Regional do Trabalho da 22ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,23,"TRT23","Tribunal Regional do Trabalho da 23ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,24,"TRT24","Tribunal Regional do Trabalho da 24ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JT,90,"CSJT","Conselho Superior da Justiça do Trabalho"));
	
		//JE
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,0,"TSE","Tribunal Superior Eleitoral"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,1,"TRE-AC","Tribunal Regional Eleitoral - Acre"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,2,"TRE-AL","Tribunal Regional Eleitoral - Alagoas"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,3,"TRE-AM","Tribunal Regional Eleitoral - Amazonas"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,4,"TRE-AP","Tribunal Regional Eleitoral - Amapá"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,5,"TRE-BA","Tribunal Regional Eleitoral - Bahia"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,6,"TRE-CE","Tribunal Regional Eleitoral - Ceará"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,7,"TRE-DF","Tribunal Regional Eleitoral - Distrito Federal"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,8,"TRE-ES","Tribunal Regional Eleitoral - Espírito Santo"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,9,"TRE-GO","Tribunal Regional Eleitoral - Goiás"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,10,"TRE-MA","Tribunal Regional Eleitoral - Maranhão"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,11,"TRE-MG","Tribunal Regional Eleitoral - Minas Gerais"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,12,"TRE-MS","Tribunal Regional Eleitoral - Mato Grosso do Sul"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,13,"TRE-MT","Tribunal Regional Eleitoral - Mato Grosso"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,14,"TRE-PA","Tribunal Regional Eleitoral - Pará"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,15,"TRE-PB","Tribunal Regional Eleitoral - Paraíba"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,16,"TRE-PE","Tribunal Regional Eleitoral - Pernambuco"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,17,"TRE-PI","Tribunal Regional Eleitoral - Piauí"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,18,"TRE-PR","Tribunal Regional Eleitoral - Paraná"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,19,"TRE-RJ","Tribunal Regional Eleitoral - Rio de Janeiro"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,20,"TRE-RN","Tribunal Regional Eleitoral - Rio Grande do Norte"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,21,"TRE-RO","Tribunal Regional Eleitoral - Rondônia"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,22,"TRE-RR","Tribunal Regional Eleitoral - Rorâima"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,23,"TRE-RS","Tribunal Regional Eleitoral - Rio Grande do Sul"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,24,"TRE-SC","Tribunal Regional Eleitoral - Santa Catarina"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,25,"TRE-SE","Tribunal Regional Eleitoral - Sergipe"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,26,"TRE-SP","Tribunal Regional Eleitoral - São Paulo"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JE,27,"TRE-TO","Tribunal Regional Eleitoral - Tocantins"));
		
		//JMU
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,0,"STM","Superior Tribunal Militar"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,1,"CJM01","Circunscrição Judiciária Militar da 1ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,2,"CJM02","Circunscrição Judiciária Militar da 2ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,3,"CJM03","Circunscrição Judiciária Militar da 3ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,4,"CJM04","Circunscrição Judiciária Militar da 4ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,5,"CJM05","Circunscrição Judiciária Militar da 5ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,6,"CJM06","Circunscrição Judiciária Militar da 6ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,7,"CJM07","Circunscrição Judiciária Militar da 7ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,8,"CJM08","Circunscrição Judiciária Militar da 8ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,9,"CJM09","Circunscrição Judiciária Militar da 9ª Região"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,10,"CJM10","Circunscrição Judiciária Militar da 10ª Região"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,11,"CJM11","Circunscrição Judiciária Militar da 11ª Região"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JMU,12,"CJM12","Circunscrição Judiciária Militar da 12ª Região"));	
		
		//JEDFT
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,1,"TJAC","Tribunal de Justiça do Acre"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,2,"TJAL","Tribunal de Justiça de Alagoas"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,3,"TJAP","Tribunal de Justiça do Amapá"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,4,"TJAM","Tribunal de Justiça do Amazonas"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,5,"TJBA","Tribunal de Justiça da Bahia"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,6,"TJCE","Tribunal de Justiça do Ceará"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,7,"TJDFT","Tribunal de Justiça do Distrito Federal e Territórios"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,8,"TJES","Tribunal de Justiça do Espírito Santo"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,9,"TJGO","Tribunal de Justiça de Goiás"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,10,"TJMA","Tribunal de Justiça do Maranhão"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,11,"TJMT","Tribunal de Justiça do Mato Grosso"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,12,"TJMS","Tribunal de Justiça do Mato Grosso do Sul"));	
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,13,"TJMG","Tribunal de Justiça de Minas Gerais"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,14,"TJPA","Tribunal de Justiça do Pará"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,15,"TJPB","Tribunal de Justiça da Paraíba"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,16,"TJPR","Tribunal de Justiça do Paraná"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,17,"TJPE","Tribunal de Justiça de Pernambuco"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,18,"TJPI","Tribunal de Justiça do Piauí"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,19,"TJRJ","Tribunal de Justiça do Rio de Janeiro"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,20,"TJRN","Tribunal de Justiça do Rio Grande do Norte"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,21,"TJRS","Tribunal de Justiça do Rio Grande do Sul"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,22,"TJRO","Tribunal de Justiça de Rondônia"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,23,"TJRR","Tribunal de Justiça de Roraima"));		
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,24,"TJSC","Tribunal de Justiça de Santa Catarina"));
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,25,"TJSP","Tribunal de Justiça de São Paulo"));	
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,26,"TJSE","Tribunal de Justiça de Sergipe"));	
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JEDFT,27,"TJTO","Tribunal de Justiça do Tocantins"));
		
		//JME
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JME,13,"TJMMG","Tribunal da Justiça Militar de Minas Gerais"));	
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JME,21,"TJMRS","Tribunal da Justiça Militar do Rio Grande do Sul"));	
		unidadesJudiciarias.add(new UnidadeJudiciaria(SegmentoJudiciario.JME,26,"TJMSP","Tribunal da Justiça Militar de São Paulo"));	
		
	}
	
	public static List<UnidadeJudiciaria> listarUnidadesJudiciarias(){
		return unidadesJudiciarias;
	}
	
	
	public static List<String> listarSegmentosJudiciarios(){
		List<SegmentoJudiciario> segmentos = Arrays.asList(SegmentoJudiciario.values());
		List<String> segmentosStr = new ArrayList<String>();
		for (SegmentoJudiciario seg: segmentos) {
			segmentosStr.add(seg.getDescricao());
		}
		return segmentosStr;
	}	
	
	public static List<UnidadeJudiciaria> listarUnidadeJudiciariaPorSigla(String sigla) {
		List<UnidadeJudiciaria> uj = new ArrayList<UnidadeJudiciaria>();
		for (UnidadeJudiciaria ujt : listarUnidadesJudiciarias()) {
			if (ujt.getSigla().equals(sigla)) {
				uj.add(ujt);
			}
		}
		return uj;
	}	
	
	public static String getSiglaSegmentoJudiciario(String descricao) {
		List<SegmentoJudiciario> segmentos = Arrays.asList(SegmentoJudiciario.values());
		for (SegmentoJudiciario seg: segmentos) {
			if (seg.getDescricao().equals(descricao)) {
				return seg.name();
			}
		}
		return null;
	}	
	
	public static List<String> listarUnidadesJudiciariasPorSegmento(String segmento){
		List<String> ujStr = new ArrayList<String>();
		for (UnidadeJudiciaria uj: listarUnidadesJudiciarias()) {
			if (uj.getSegmentoJudiciario().getDescricao().equals(segmento)) {
				ujStr.add(uj.getNome());
			}
		}
		return ujStr;
	}	
	
	public static String getSiglaUnidadeJudiciariaPorDescricao(String descricao) {
		for (UnidadeJudiciaria uj: listarUnidadesJudiciarias()) {
			if (uj.getNome().equals(descricao)) {
				return uj.getSigla();
			}
		}		
		return "";
	}	
	
}

