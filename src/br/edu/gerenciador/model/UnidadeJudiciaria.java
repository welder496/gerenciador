package br.edu.gerenciador.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;

public class UnidadeJudiciaria {

	private static final List<UnidadeJudiciaria> unidadesJudiciarias = new ArrayList<UnidadeJudiciaria>();	
	
	private SegmentoJudiciario segmentoJudiciario;
	private int codigo;
	private String sigla;
	private String nome;

	/**
	 * Due to Jackson
	 */
	public UnidadeJudiciaria() {}
	
	public UnidadeJudiciaria(SegmentoJudiciario seg, int codigo, String sigla,String nome) {
		Validate.notNull(seg,"Segmento Judiciário não pode ser nulo!!");
		Validate.notNull(codigo,"Código da Unidade Judiciária não pode ser nulo!!");
		Validate.notBlank(sigla,"Sigla não pode ficar em branco!!");
		Validate.notBlank(nome,"Nome não pode ficar em branco!!");
		this.segmentoJudiciario=seg;
		this.codigo=codigo;
		this.sigla=sigla;
		this.nome=nome;
	} 	
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getSigla() {
		return sigla;
	}

	public String getNome() {
		return nome;
	}
	
	public SegmentoJudiciario getSegmentoJudiciario() {
		return segmentoJudiciario;
	}

	public List<UnidadeJudiciaria> getUnidadesJudiciariasBySegmento(SegmentoJudiciario segmento){
		List<UnidadeJudiciaria> temp = new ArrayList<UnidadeJudiciaria>();
		for (UnidadeJudiciaria uj: unidadesJudiciarias) {
			if (uj.getSegmentoJudiciario().getDescricao().equals(segmento.getDescricao())) {
				temp.add(uj);
			}
		}
		return temp;
	}
	
}
