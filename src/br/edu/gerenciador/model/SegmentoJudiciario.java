package br.edu.gerenciador.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 
 * @author welder
 * Resolução 65-CNJ
 * 
 */
public enum SegmentoJudiciario {

	STF(1,"Supremo Tribunal Federal"),  //Supremo Tribunal Federal
	CNJ(2,"Conselho Nacional de Justiça"),  //Conselho Nacional de Justiça
	STJ(3,"Superior Tribunal de Justiça"),  //Superior Tribunal de Justiça
	JF(4,"Justiça Federal"),   //Justiça Federal
	JT(5,"Justiça do Trabalho"),   //Justiça do Trabalho
	JE(6,"Justiça Eleitoral"),   //Justiça Eleitoral
	JMU(7,"Justiça Militar da União"),  //Justiça Militar da União
	JEDFT(8,"Justiça dos Estados e do Distrito Federal"), //Justiça dos Estados e do Distrito Federal e Territórios
	JME(9,"Justiça Militar Estadual"),  //Justiça Militar Estadual
	AC(10,"Autoridade Certificadora"); //Autoridade Certificadora
	
	private int codigo;
	
	private String descricao;	
	
	public int getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	private SegmentoJudiciario(int codigo,String descricao){
		this.codigo=codigo;
		this.descricao=descricao;
	}
	
	public static List<String> listarSegmentosJudiciarios(){
		List<SegmentoJudiciario> segmentos = Arrays.asList(SegmentoJudiciario.values());
		List<String> segmentosStr = new ArrayList<String>();
		for (SegmentoJudiciario seg: segmentos) {
			segmentosStr.add(seg.getDescricao());
		}
		return segmentosStr;
	}		
	
}
