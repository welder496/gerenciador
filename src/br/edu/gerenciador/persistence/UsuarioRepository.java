package br.edu.gerenciador.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.edu.gerenciador.model.Usuario;

public class UsuarioRepository {
	
	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("gerenciador");
	private EntityManager em = factory.createEntityManager();
	
	public Usuario getUsuario(String nome, String senha) {
		try {
			em.getTransaction().begin();
			Query query = em.createQuery("select u from Usuario u where u.nome=:nome and senha=:senha");
			query.setParameter("nome",nome);
			query.setParameter("senha", senha);
			Usuario usuario = (Usuario) query.getSingleResult();
			return usuario;
		} catch (NoResultException e) {
			return null;
		} finally {
			em.getTransaction().commit();
		}
	}

}
